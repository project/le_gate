<?php
/**
 * @file
 * le_gate.module
 */

define ('LE_GATE_PATH', 'le-gate');
/*define ('LE_GATE_COOKIE_PATH', 'le-gate/set-cookie');*/
define('LE_GATE_EVAL_PATH', 'le-gate/eval-path');
define('LE_GATE_EVAL_DATE', 'le-gate/eval-date');
define ('LE_GATE_ADMIN_PATH', 'admin/config/system/le-gate');

/*
function le_gate_init() {
  // Check paths.
  $current_path = current_path();
  // If this is the le-gate path return.
  if (($current_path == LE_GATE_PATH) || ($current_path == LE_GATE_COOKIE_PATH) || ($current_path == LE_GATE_ADMIN_PATH)) {
    return;
  }
  // If admin page and configured to skip admin pages, return.
  $skip = variable_get('le_gate_skip_admin', 1);
  if ($skip && path_is_admin($current_path)) {
    return;
  }
  // If cookie already set, return.
  if (cookie_monster_cookie_exists(LE_GATE_COOKIE_ID)) {
    return;
  }
  $paths = variable_get('le_gate_paths', '');
  // If excluded just return FALSE.
  if (($paths == '~') || drupal_match_path("~$current_path", $paths)) {
    return;
  }
  // Check if included, if not return FALSE.
  if ((!($paths == '')) && !drupal_match_path($current_path, $paths)) {
    return;
  }
  // Look for drush otherwise do the redirect.
  if (!drupal_is_cli()) {
    drupal_goto('le-gate');
  }
}
*/

 /**
 * Implements hook_permission().
 */
function le_gate_permission() {
  $permissions = array();
  $permissions['administer le gate'] = array(
    'title' => t('Administer Le Gate'),
    'description' => t('Change Le gate settings'),
   );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function le_gate_menu() {
  $items = array();
  $items[LE_GATE_ADMIN_PATH] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => t('Le Gate'),
    'description' => t('Administer Le Gate'),
    'access arguments' => array('administer le gate'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('le_gate_settings_form'),
    'file' => 'includes/le_gate.admin.inc',
  );
  $items[LE_GATE_PATH] = array(
    'type' => MENU_CALLBACK,
    'title callback' => 'le_gate_page_title',
    'access arguments' => array('access content'),
    'page callback' => 'le_gate_page',
    'file' => 'includes/le_gate.pages.inc',
  );
  /*
  $items[LE_GATE_COOKIE_PATH] = array(
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
    'page callback' => 'le_gate_set_cookie',
    'file' => 'includes/le_gate.pages.inc',
  );
  */
  $items[LE_GATE_EVAL_PATH] = array(
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
    'page callback' => 'le_gate_eval_path_is_gated',
    'file' => 'includes/le_gate.pages.inc',
  );
  $items[LE_GATE_EVAL_DATE] = array(
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
    'page callback' => 'le_gate_eval_date',
    'file' => 'includes/le_gate.pages.inc',
  );
  return $items;
}

/**
 * Title callback for Gate page.
 */
function le_gate_page_title() {
  $title = variable_get('le_gate_page_title', t('Le Gate'));
  return $title;
}

/**
 * Implements hook_block_info().
 */
function le_gate_block_info() {
  $blocks = array();
  $blocks['le_gate_gate'] = array(
    'info' => t('Le Gate: Gate Block'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function le_gate_block_view($delta) {
  $block = array();
  switch ($delta) {
    case 'le_gate_gate':
      $block['subject'] = '';
      $block['content'] = theme('le_gate_gate');
      return $block;
      break;
  }
}

/**
 * Implements hook_theme().
 */
function le_gate_theme() {
  $themes = array();
  $themes['le_gate_gate'] = array(
    'variables' => array(),
    'template' => 'le-gate-gate',
    'file' => 'theme/le_gate.theme.inc',
  );
  return $themes;
}


/**
 * Set a cookie for Le Gate access.
 * @param string $redirect_path
 *   Path to redirect to after cookies is set.
 */
function le_gate_set_cookie($redirect_path = '') {
  if (isset($_GET['le_gate_redirect'])) {
    $redirect_path = $_GET['le_gate_redirect'];
  }
  $data = array(
    'id' => LE_GATE_COOKIE_ID,
    'expire_time' => 86400,
  );
  cookie_monster_set_cookie($data, FALSE);
  drupal_goto($redirect_path);
}

/**
 * Gets a timestamp from a date array.
 * @param $array
 *   The date array to convert to a timestamp.
 * @return int
 *   A unix timestamp.
 */
function le_gate_get_date_stamp($array) {
  return strtotime("{$array['month']}/{$array['day']}/{$array['year']}");
}

/**
 * Sets the configured message.
 */
function le_gate_set_message() {
  $message = variable_get('le_gate_message', t('Sorry but you did not pass.'));
  drupal_set_message(t($message), 'error');
}

/**
 * Form for selecting a date.
 */
function le_gate_date_form($form, &$form_state) {
  $form['le_gate_selected_date'] = array(
    '#type' => 'date',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Date select form submit handler.
 */
function le_gate_date_form_submit($form, $form_state) {
  // Need this function for the form submit but does nothing;
  return;
  /*
  // Get user entered date.
  $check_date = le_gate_get_date_stamp($form_state['values']['le_gate_selected_date']);
  // Get type of date comparision.
  $type = variable_get('le_gate_date_type', 'before');
  // Get the redirect url for after check.
  $redirect_url = variable_get('le_gate_date_redirect', '');
  // Get timestamp for configured date 1.
  $date1 = le_gate_get_date_stamp(variable_get('le_gate_date_1'));
  switch ($type) {
    case 'before':
      if ($check_date < $date1) {
        le_gate_set_cookie($redirect_url);
      }
      else {
        le_gate_set_message();
      }
      break;
    case 'after':
      if ($check_date > $date1) {
        le_gate_set_cookie($redirect_url);
      }
      else {
        le_gate_set_message();
      }
      break;
    case 'between':
      // Get timestamp for configured date 2.
      $date2 = le_gate_get_date_stamp(variable_get('le_gate_date_2'));
      if (($date1 < $check_date) && ($check_date < $date2)) {
        le_gate_set_cookie($redirect_url);
      }
      else {
        le_gate_set_message();
      }
      break;
  }*/
}
