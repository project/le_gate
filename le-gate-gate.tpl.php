<?php
/**
 * @file
 * le-gate-gate.tpl.php
 * @see template_preprocess_le_gate_gate().
 */
?>

<div class="le-gate-page">
  <?php print render($content); ?>
</div>
